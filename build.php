<?php

class PharBuilder {
  /** @var PharSetting[] */
  var $settings = [];

  function __construct() {
    $this->settings = $this->getSettings();
  }

  function getSettings() {
    $file = __DIR__ . '/build.ini';
    $data = [];

    foreach (parse_ini_file($file, true) as $name => $item) {
      if (false == is_array($item)) {
        continue;
      }

      $object = new PharSetting($item);

      if ($object->vaildate()) {
        $data[$name] = $object;
      }
    }

    return $data;
  }

  static function console() {
    if (false == Phar::canWrite()) {
      die("Phar is in read-only mode, try php -d phar.readonly=0 build.php \n");
    }

    $builder = new PharBuilder();
    $data = $builder->buildAll();

    foreach ($data as $name => $message) {
      echo "$name : $message \n";
    }
  }

  function buildAll() {
    $data = [];

    foreach (array_keys($this->settings) as $name) {
      $data[$name] = $this->build($name);
    }

    return $data;
  }

  function build($name) {
    $input = __DIR__ . '/' . $name;
    $output = $input . '.phar';

    if (false == is_dir($input)) {
      return 'Input directory is not exists.';
    }

    if (file_exists($output) && (false == unlink($output))) {
      return 'Can not remove old phar file.';
    }

    $setting = $this->settings[$name];
    $alias = $name . '.phar';
    $phar = new Phar($output, 0, $alias);
    $phar->buildFromDirectory($input);
    $phar->startBuffering();

    if ('' != $setting->stub) {
      $phar->setStub($this->getStub($name));
    }

    $phar->stopBuffering();
    return 'Build phar file finished.';
  }

  function getStub($name) {
    $stub = $this->stubWeb($name);
    return $stub;
  }

  function stubWeb($name) {
    $setting = $this->settings[$name];
    $index = $setting->index;
    $alias = $name . '.phar';

    $stub = <<< EOD
<?php
ini_set('allow_url_include', 'On');
Phar::interceptFileFuncs();
Phar::webPhar('$alias', '$index');
__HALT_COMPILER();
EOD;

    //echo "$stub \n";
    return $stub;
  }
}

class PharSetting {
  var $index = 'index.php';
  var $stub = '';

  function __construct(array $setting) {
    $keys = array_keys(get_object_vars($this));

    foreach ($keys as $index) {
      $item = &$setting[$index];

      if (isset($item)) {
        $this->{$index} = $item;
      }
    }
  }

  function vaildate() {
    return true;
  }
}

PharBuilder::console();
